let path = require('path');
require('dotenv').config({path: path.join(__dirname,'.env')});

module.exports = {
    host: process.env.HOST,
    user: process.env.USER_NAME,
    password: process.env.PASSWORD,
    database:  process.env.DATABASE_NAME
};