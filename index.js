const express = require('express');
const cors = require("cors"); 
const { v4: uuidv4 } = require('uuid');
const app = express();
const db = require('./database/db');
const port = process.env.PORT || 5000;
const {check, validationResult} = require('express-validator');
const moment = require('moment');
const seed = require('./seed/seed');
const { mail } = require('./mail/mail');

app.use(express.json());
app.use(cors());


app.post('/events', [
  check('event_name').notEmpty(),
  check('event_organiser').notEmpty(),
  check('event_organiser_email').notEmpty().isEmail(),
  check('event_date').notEmpty(),
  check('event_cost').notEmpty(),
  check('location').notEmpty().isAlpha()
], async (req,res) => {
    try {
      const errors = validationResult(req);
      if(!errors.isEmpty() && moment(`${req.body.event_date}`, "YYYY-MM-DD").isValid()) {
        return res.status(400).json({ errors: errors.array() })
      }
      const {event_name, event_organiser,event_organiser_email, event_date, event_cost, location } = req.body;
      const id = uuidv4();
      const newEvent = await db.runQuery(`INSERT INTO EVENT (event_id, event_name, event_organiser, event_organiser_email, event_date, event_cost, location) VALUES ('${id}' , '${event_name}', '${event_organiser}', '${event_organiser_email}', '${event_date}', '${event_cost}', '${location}')`);
      res.json(newEvent);
    } catch(err) {
      res.status(500);
      console.error(err);
    }
})


app.get('/events', async(req,res) => {
  try {
      const allEvents = await db.runQuery('SELECT * FROM EVENT');
      res.status(200);
      res.json(allEvents);
  } catch(err) {
    res.status(500);
    console.error(err);
  }
})

app.get('/events/:id', async (req,res) => {
  try {
    let {id} = req.params;
    if(id === 'past') {
      const pastEvents = await db.runQuery('SELECT * FROM EVENT WHERE EVENT_DATE < CURDATE()');
      res.json(pastEvents);
    } else if(id === 'future') {
      const futureEvents = await db.runQuery('SELECT * FROM EVENT WHERE EVENT_DATE >= CURDATE()');
      res.json(futureEvents);
    } else {
      const idSearch= await db.runQuery(`SELECT * FROM EVENT WHERE EVENT_ID = '${id}'`);
      res.status(200);
      res.json(idSearch);
    }
  } catch(err){
    console.error(err);
    res.status(500);
    res.send(err);
  }
});

app.delete('/events/:id', async (req,res) => {
  try {
    const { id } = req.params;
    let getEventToDelete = await db.runQuery(`SELECT event_name FROM event WHERE event_id = '${id}'`);
    let eventToDelete = getEventToDelete[0].event_name;
    let participantEmail = await db.runQuery(`SELECT participant_email FROM event_participants WHERE event_name = '${eventToDelete}'`);
    let emailList = participantEmail.map(e => e.participant_email );
    console.log(emailList);
    mail(emailList);
    let deleteItem = await db.runQuery(`DELETE FROM event WHERE event_id = '${id}'`);
    res.json(deleteItem);
  } catch(err) {
    console.error(err);
    res.status(500);
    res.send(err);
  }
})


app.put('/events/update/:id', [
  check('event_date').notEmpty(),
  check('event_cost').notEmpty(),
  check('location').notEmpty().isAlpha()
], async (req,res) => {
  try {
    const errors = validationResult(req);
    if(!errors.isEmpty() && moment(`${req.body.event_date}`, "YYYY-MM-DD").isValid()) {
      return res.status(400).json({ errors: errors.array() })
    }
    const { id } = req.params; 
    const location = req.body.location;
    const cost  = req.body.event_cost;
    let updateEvent = await db.runQuery(`UPDATE event SET event_cost = '${cost}', location = '${location}' WHERE event_id='${id}'`);
    res.json(updateEvent);
  } catch(err) {
    console.error(err);
    res.status(500);
    res.send(err);
  }
})


app.post( "/events/booking", [
  check('eventName').notEmpty(),
  check('participantName').notEmpty(),
  check('email').notEmpty().isEmail()
], async (req,res) => {
  try {
  const errors = validationResult(req);
  if(!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }
  let data = req.body;
  const id = uuidv4();
  const name = data.eventName;
  const participant = data.participantName;
  const email = data.email;
  let booking = await db.runQuery(`INSERT INTO event_participants (participant_id, event_name, participant_name,participant_email) VALUES ('${id}' , '${name}', '${participant}', '${email}')`);
  res.status(200);
  res.json(booking);
  } catch(err) {
  console.error(err);
  res.status(500);
  res.send(err);
  }
})

app.listen(port, () => {
    `Server started in port ${port}`;
});
