const config = require('../config');

let createDatabaseQuery = `CREATE DATABASE IF NOT EXISTS ${config.database}`

let setDatabaseQuery  = `USE ${config.database}`

let createTableEvent = `CREATE TABLE IF NOT EXISTS event (
    event_id VARCHAR(36) PRIMARY KEY,
    event_name VARCHAR(50) NOT NULL UNIQUE,
    event_organiser TEXT NOT NULL,
    event_organiser_email TEXT NOT NULL,
    event_date DATE NOT NULL,
    event_cost DECIMAL(6,2) NOT NULL,
    location TEXT NOT NULL
);`

let createTableParticipant = `CREATE TABLE IF NOT EXISTS event_participants(
    participant_id  VARCHAR(36) PRIMARY KEY,
    event_name VARCHAR(50),
    participant_name TEXT,
    participant_email TEXT NOT NULL,
    FOREIGN KEY(event_name) REFERENCES event(event_name) ON DELETE CASCADE
);`

let loadDummyEventData = `LOAD DATA LOCAL INFILE './csv/test.csv' INTO TABLE event FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;`
let loadDummyParticipantData = `LOAD DATA LOCAL INFILE './csv/test2.csv' INTO TABLE event_participants FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;`
let eventTableCheck = 'SELECT COUNT(*) AS events FROM event';

module.exports = {
    createDatabaseQuery,
    setDatabaseQuery ,
    createTableEvent,
    createTableParticipant,
    loadDummyEventData,
    loadDummyParticipantData,
    eventTableCheck
}