const converter = require('json-2-csv');
const faker = require('faker');
const fs = require("fs");

let events = [];
Array.from(Array(10).keys()).forEach(item => {
    events.push({
        "event_id": faker.random.uuid(),
        "event_name": faker.random.word(),
        "event_organiser": faker.company.companyName(),
        "event_organiser_email": faker.internet.email(),
        "event_date": '2021-09-12',
        "event_cost": 1000.00,
        "location": faker.address.city()
    });
});

converter.json2csv(events, (err, csv) => {
    if (err) {
        throw err;
    } else if (!fs.existsSync('./csv')) {
        fs.mkdirSync('./csv');
        fs.writeFile('./csv/test.csv', csv, function (err) {
            if (err) return console.log(err);
        });
    }
});

let participants = [];
Array.from(Array(10).keys()).forEach(item => {
    participants.push({
        "participant_id": faker.random.uuid(),
        "event_name": events[item].event_name,
        "participant_name": faker.internet.userName(),
        "participant_email": faker.internet.email()
    });
});

converter.json2csv(participants, (err, csv) => {
    if (err) {
        throw err;
    } else {
        fs.writeFile('./csv/test2.csv', csv, function (err) {
            if (err) return console.log(err);
        });
    }
});
