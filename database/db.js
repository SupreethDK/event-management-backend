const config = require("../config");
const mysql = require("mysql");
const sqlQuery = require('../models/model');

let connection = mysql.createPool({
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database,
});


function runQuery (query) {
    return new Promise((resolve, reject) => {
        connection.query(query, function (err, result) {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
    })
  })
  }


(async () => {
        try{
            const createDatabase = await runQuery(sqlQuery.createDatabaseQuery);
            const selectTable = await runQuery(sqlQuery.setDatabaseQuery);
            const  createTableEvent  = await runQuery(sqlQuery.createTableEvent);
            const createTableParticipant = await runQuery(sqlQuery.createTableParticipant);
        } catch(err) {
          console.error(err);
        }   
})();
    

module.exports = {
    connection, runQuery
};
